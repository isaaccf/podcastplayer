import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLocalStorage from 'vue-ls'
import $ from 'jquery'

Vue.use(VueAxios, axios)

const cacheable = true
var options = {
  namespace: 'vuejs__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
}
Vue.use(VueLocalStorage, options)

const timeCached = 86400000 // expires in 24 hour

// before a request is made
axios.interceptors.request.use(request => {
  // Only cache GET requests
  if (request.method === 'get' && cacheable) {
    let url = request.url

    // Append the params, I use jquery param but you can change to whatever you use
    if (request.params) {
      url += '?' + $.param(request.params)
    }

    const _cached = Vue.ls.get(url)

    if (_cached) {
      _cached.__fromCache = true
      request.__fromCache = true
      request.data = _cached

      // Set the request adapter to send the cached response and prevent the request from actually running
      request.adapter = () => {
        return Promise.resolve({
          data: _cached,
          status: request.status,
          statusText: request.statusText,
          headers: request.headers,
          config: request,
          request: request
        })
      }
    }
  }
  return request
})

// before a response is returned
axios.interceptors.response.use(response => {
  // if you dont want to cache a specific url, send a param `__cache = false`
  const isCacheable = !response.config.params || (response.config.params && response.config.params.__cache !== false)

  if (cacheable && isCacheable) {
    let url = response.config.url

    if (response.config.params) {
      url += '?' + $.param(response.config.params)
    }

    if (response.config.method === 'get') {
      // On get request, store the response in the cache
      Vue.ls.set(url, response.data, timeCached)
    }
  }
  return response
})

export {getPodcastList, getPodcastDetail, getEpisodeDetail, getRss}

/*
Get first 100 podcasts from iTunes
*/
function getPodcastList () {
  const url = 'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json'
  return axios.get(url).then(response => response.data.feed.entry).catch(e => console.error(`Error - getPodcastList(): ${e.message}`))
}

/*
Get podcast detail using podcast id
*/
function getPodcastDetail (podcastId) {
  const url = `https://cors.io/?https://itunes.apple.com/lookup?id=${podcastId}`
  return axios.get(url).then(response => response.data).catch(e => console.error(`Error - getPodcastDetail(${podcastId}): ${e.message}`))
}

/*
Get RSS XML with episodes
*/
function getRss (url) {
  const request = `https://cors.io/?${url}`
  return axios.get(request).then(response => {
    var json = response.data
    if (!response.config.__fromCache) {
      json = parseRss2Json(response.data)
      Vue.ls.set(request, json, timeCached)
    }
    return json
  }).catch(e => console.error(`Error - getPodcastDetail(${url}): ${e.message}`))
}

/*
Get all information about a episode
*/
function getEpisodeDetail (idEpisode, urlRss) {
  return getRss(urlRss).then(data => {
    var result = {}
    result['podcastInformation'] = data.podcastInformation
    result['episode'] = data.episodes.filter(item => item.guid === idEpisode)[0]
    return result
  })
}

/*
Parse RSS XML to custom Json with episodes details and podcast detail
*/
function parseRss2Json (xml) {
  var domParser = new DOMParser()
  var document = domParser.parseFromString(xml, 'application/xml')
  var json = {podcastInformation: {}, episodes: []}

  var episodes = Array.prototype.slice.call(document.getElementsByTagName('item'))
  episodes.forEach(function (episode) {
    json.episodes.push({
      guid: getNodeValue(episode, 'guid'),
      title: getNodeValue(episode, 'title'),
      pubDate: getNodeValue(episode, 'pubDate'),
      description: getNodeValue(episode, 'description'),
      url: getAttrValue(episode, 'enclosure', 'url'),
      summary: getNodeValue(episode, 'summary') || getNodeValue(episode, 'itunes:summary'),
      duration: getNodeValue(episode, 'duration') || getNodeValue(episode, 'itunes:duration')
    })
  })

  var channel = document.getElementsByTagName('channel')
  json.podcastInformation['title'] = getNodeValue(channel[0], 'title')
  json.podcastInformation['description'] = getNodeValue(channel[0], 'description')
  json.podcastInformation['author'] = getNodeValue(channel[0], 'author')
  json.podcastInformation['image'] = getNodeValue(channel[0], 'image')

  return json
}

function getNodeValue (item, name) {
  var node = item.getElementsByTagName(name)[0]
  return node !== undefined && node.childNodes[0] !== undefined ? node.childNodes[0].nodeValue : ''
}

function getAttrValue (item, name, attr) {
  var node = item.getElementsByTagName(name)[0]
  return node !== undefined ? node.getAttribute(attr) : ''
}
