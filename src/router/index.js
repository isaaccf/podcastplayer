import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/List'
import Podcast from '@/components/Podcast'
import Episode from '@/components/Episode'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'List',
      component: List
    },
    {
      path: '/podcast/:idPodcast',
      name: 'Podcast',
      component: Podcast
    },
    {
      path: '/podcast/:idPodcast/episode/:idEpisode',
      name: 'Episode',
      component: Episode
    }
  ]
})
