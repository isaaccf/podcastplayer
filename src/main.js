// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'buefy/lib/buefy.css'
import moment from 'moment'
import Sidebar from './components/Sidebar'

Vue.config.productionTip = false

Vue.filter('moment', function (date) {
  return moment(date).format('DD/MM/YYYY')
})

Vue.component('sidebar', Sidebar)

export const loading = new Vue()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  render: h => h(App)
})
